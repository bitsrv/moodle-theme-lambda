<?php

class lambda_user_picture extends user_picture {

    public function get_url(moodle_page $page, renderer_base $renderer = null) {
        global $DB;

        $user = $this->user;
        if (empty($user->username)) {
            $user = $DB->get_record('user', array('id' => $user->id));
        }

        $url = 'http://weixin.info.bit.edu.cn/api/v1/getOrgPersonAvatar?badgenumber='.$user->username;
        // if (!empty($this->size)) {
        //     $url .= '&size='.$this->size;
        // }
        
        $rst = lambda_diapi_get($url);
        $json = json_decode($rst);
        if (!empty($json->avatar)) {
            return $json->avatar;
        } else {
            return parent::get_url($page, $renderer);
        }
    }
}

